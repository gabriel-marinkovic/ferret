package templates

import (
	"fmt"
	"gitlab.com/gabriel-m/ferret/pkg/drivers"
)

func Blur() string {
	return `
		(el) => {
			el.blur()
		}
	`
}

func BlurBySelector(selector string) string {
	return fmt.Sprintf(`
		(parent) => {
			const el = parent.querySelector('%s');

			if (el == null) {
				throw new Error('%s')
			}

			el.blur();
		}
`, selector, drivers.ErrNotFound)
}
