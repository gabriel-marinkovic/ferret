package templates

import (
	"fmt"

	"gitlab.com/gabriel-m/ferret/pkg/drivers"
	"gitlab.com/gabriel-m/ferret/pkg/drivers/cdp/eval"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
)

func WaitBySelectorAll(selector values.String, when drivers.WaitEvent, value core.Value, check string) string {
	return fmt.Sprintf(`
			var elements = document.querySelectorAll(%s); // selector

			if (elements == null || elements.length === 0) {
				return false;
			}

			var resultCount = 0;

			elements.forEach((el) => {
				var result = %s; // check

				// when
				if (result %s %s) {
					resultCount++;
				}
			});

			if (resultCount === elements.length) {
				return true;
			}

			// null means we need to repeat
			return null;
	`,
		eval.ParamString(selector.String()),
		check,
		WaitEventToEqOperator(when),
		eval.Param(value),
	)
}
