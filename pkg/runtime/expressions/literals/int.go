package literals

import (
	"context"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
)

type IntLiteral int

func NewIntLiteral(value int) IntLiteral {
	return IntLiteral(value)
}

func (l IntLiteral) Exec(_ context.Context, _ *core.Scope) (core.Value, error) {
	return values.NewInt(int(l)), nil
}
