package datetime

import (
	"context"

	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"

	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
)

// Now returns new DateTime object with Time equal to time.Now().
// @returns (DateTime) - New DateTime object.
func Now(_ context.Context, args ...core.Value) (core.Value, error) {
	err := core.ValidateArgs(args, 0, 0)
	if err != nil {
		return values.None, err
	}

	return values.NewCurrentDateTime(), nil
}
