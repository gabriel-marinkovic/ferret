package html

import (
	"context"

	"gitlab.com/gabriel-m/ferret/pkg/drivers"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
)

// SCROLL_TOP scrolls the document's window to its top.
// @param doc (HTMLDocument) - Target document.
func ScrollTop(ctx context.Context, args ...core.Value) (core.Value, error) {
	err := core.ValidateArgs(args, 1, 1)

	if err != nil {
		return values.None, err
	}

	doc, err := drivers.ToDocument(args[0])

	if err != nil {
		return values.None, err
	}

	return values.None, doc.ScrollTop(ctx)
}
