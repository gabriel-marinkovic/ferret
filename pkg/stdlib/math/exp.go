package math

import (
	"context"
	"math"

	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values/types"
)

// Exp returns Euler's constant (2.71828...) raised to the power of value.
// @param number (Int|Float) - Input number.
// @returns (Float) - Euler's constant raised to the power of value.
func Exp(_ context.Context, args ...core.Value) (core.Value, error) {
	err := core.ValidateArgs(args, 1, 1)

	if err != nil {
		return values.None, err
	}

	err = core.ValidateType(args[0], types.Int, types.Float)

	if err != nil {
		return values.None, err
	}

	return values.NewFloat(math.Exp(toFloat(args[0]))), nil
}
