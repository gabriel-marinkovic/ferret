package math

import (
	"context"
	"math"

	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
)

// Pi returns Pi value.
// @returns (Float) - Pi value.
func Pi(_ context.Context, args ...core.Value) (core.Value, error) {
	err := core.ValidateArgs(args, 0, 0)

	if err != nil {
		return values.None, err
	}

	return values.NewFloat(math.Pi), nil
}
