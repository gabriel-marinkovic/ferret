package utils

import "gitlab.com/gabriel-m/ferret/pkg/runtime/core"

func RegisterLib(ns core.Namespace) error {
	return ns.RegisterFunctions(core.Functions{
		"WAIT":  Wait,
		"PRINT": Print,
	})
}
